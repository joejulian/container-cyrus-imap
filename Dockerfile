FROM registry.gitlab.com/joejulian/docker-arch:latest as build
RUN pacman -Syu --needed --noconfirm \
    base-devel \
    git \
    go \
    perl-module-install \
    python-pip \
    sudo

RUN GO111MODULE=on GOPATH=/go GOBIN=/go/bin go install gitlab.com/joejulian/fsyslog@v0.3.0

COPY nobody.sudo /etc/sudoers.d/nobody
RUN chmod 400 /etc/sudoers.d/nobody

RUN mkdir /build && chown nobody:nobody /build
WORKDIR /build
USER nobody
ENV HOME /tmp

RUN git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si --noconfirm
# RUN curl -sLo libcap-2.27-2-x86_64.pkg.tar.xz https://archive.archlinux.org/packages/l/libcap/libcap-2.27-2-x86_64.pkg.tar.xz && \
#     yay -U --noconfirm libcap-2.27-2-x86_64.pkg.tar.xz
RUN yay -Syu --noconfirm
RUN yay -S --noconfirm sphinx
RUN yay -S --noconfirm lmdb
RUN yay -S --noconfirm krb5
RUN yay -S --noconfirm zephyr
RUN yay -S --noconfirm perl-pod-pom-view-restructured
RUN yay -S --noconfirm cyrus-sasl-gssapi
RUN yay -S --noconfirm cyrus-imapd

FROM registry.gitlab.com/joejulian/docker-arch:latest
VOLUME /config
VOLUME /data
COPY --from=build /tmp/.cache/yay/*/*.pkg.tar.zst /install/
COPY --from=build /go/bin/fsyslog /bin
COPY services /etc/services
RUN pacman -Syu --noconfirm --disable-sandbox && \
    pacman -U --noconfirm --disable-sandbox /install/* && \
    pacman -Sc --disable-sandbox --noconfirm && \
    rm -rf /var/cache/pacman/pkg
CMD fsyslog -command /usr/lib/cyrus/master
